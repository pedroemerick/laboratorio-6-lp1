/**
* @file     menu.cpp
* @brief    Implementacao da funcao que imprime o menu do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    16/05/2017
* @date	    16/05/2017
*/

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include "menu.h"

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu)
{
    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                  Menu Principal                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Inserir elemento                          *" << endl;
    cout << "*      2) Remover elemento                          *" << endl;
    cout << "*      3) Verificar se valor existe na lista        *" << endl;
    cout << "*      4) Mostrar lista                             *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Sair                                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> menu;
    cout << endl;

    return menu;
}