/**
* @file     main2.cpp
* @brief    Programa que faz algumas manipulacoes com uma lista
* @author   Pedro Emerick (p.emerick@live.com)
* @since    16/05/2017
* @date	    16/05/2017
*/

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "ll_dupla_ord.h"
#include "menu.h"

/** 
 * @brief Função principal
 */
int main ()
{
    Lista <int> lista;

    int menu = -1;

    int el;

    while (menu != 0) {

        menu = menu_principal (menu);

        if (menu == 1)
        {
            // Inseri um elemento na lista
            cout << "Digite o elemento que deseja inserir na lista: ";
            cin >> el;
            lista.Inserir (el);
        }
        else if (menu == 2)
        {
            // Remove um elemento da lista
            cout << "Digite o elemento que deseja remover da lista: ";
            cin >> el;
            lista.Remover (el);
        }
        else if (menu == 3) 
        {
            // Verifica se um elemento existe na lista
            cout << "Digite o elemento que deseja verificar se pertence a lista: ";
            cin >> el;
            
            if (lista.ExisteElemento (el) == true)
                cout << "O elemento pertence a lista !!!" << endl;
            else
                cout << "O elemento não pertence a lista !!!" << endl;                
        }
        else if (menu == 4)
        {
            // Mostra os elementos da lista
            lista.Mostrar ();
            cout << endl;
        }
        else if (menu < 0 || menu > 4)
        {
            cout << "Opção inválida !!! Digite novamente." << endl;
        }
    }

    return 0;
}