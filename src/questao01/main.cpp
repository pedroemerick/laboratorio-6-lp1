/**
* @file     main.cpp
* @brief    Programa que verifica se um conjunto de caracteres é palindromo atraves da estrutura de Pilha
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    14/05/2017
*/

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <sstream>
using std::ws;

#include <string>
using std::string;

#include <cstring>

#include "trata_string.h"
#include "palindromo_pilha.h"

/** 
 * @brief Função principal
 */
int main ()
{
    string palavra_s;
    
    cout << "Digite a palavra que deseja verificar se é palindromo: " << endl;
    cout << "--> ";
    getline (cin >> ws, palavra_s);

    string tratada_s = palavra_s;

    tratamento_string (tratada_s);

    if (palindromo_pilha (tratada_s))
        cout << endl << "'" << palavra_s << "' é um palindromo." << endl;
    else 
        cout << endl << "'" << palavra_s << "' não é um palindromo." << endl;

    return 0;
}