/**
* @file     trata_string.cpp
* @brief    Implementacao das funcoes que fazem o tratamento de strings
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    14/05/2017
*/

#include "trata_string.h"

#include <cctype>

#include <string>
using std::string;

/**
 * @brief Funcao que converte todos os caracteres para letra minuscula
 * @param s String a ser convertida
 * @param t Tamanho da string
 */
void to_minusculo (string &s, int t)
{
    for (int ii = 0; ii < t; ii++)
    {
        char aux = tolower(s[ii]);
        s[ii] = aux;
    }
}

/**
 * @brief Funcao que remove espacos e sinais de pontuacao de uma string
 * @param s String com espacos e sinais de pontuacao a remover
 * @param t Tamanho da string
 */
void retira_especiais (string &s, int t)
{
    for (int ii = 0; ii < t; ii++)
    {
        if (ispunct (s[ii]) || isspace (s[ii]))
        {
            s.erase (ii, 1);
            
            ii --;
        }
    }
}

/**
 * @brief Funcao que remove os acentos dos caracteres de uma string
 * @param s String com acentos a remover
 */
void retira_acentos (string &s)
{
    string com_acento = "âÂàÀáÁãÃêÊèÈéÉẽẼîÎìÌíÍõÕôÔòÒóÓüÜûÛúÚùÙçÇ";

    string sem_acento = "aaaaaaaaeeeeeeeeiiiiiioooooooouuuuuuuucc";

    int temp = com_acento.length ();

    int kk = 0;

    for (int ii = 0; s [ii] != '\0'; ii++) 
    {
        kk = 0;

        for (int jj = 0; jj < temp; jj += 2) 
        {
            if ((s [ii] == com_acento [jj]) && (s [ii+1] == com_acento [jj+1]))
            {   
                s [ii] = sem_acento [kk];
                
                s.erase ((ii+1), 1);
                
                ii += 1;

                break;
            }

            kk++;
        }
    }
}

/**
 * @brief Funcao que faz o tratamento da string
 * @param s String para tratamento
 */
void tratamento_string (string &s)
{
    retira_acentos (s);

    int tam_s = s.length (); // Tamanho da string sem acentos

    to_minusculo (s, tam_s);

    retira_especiais (s, tam_s);
}