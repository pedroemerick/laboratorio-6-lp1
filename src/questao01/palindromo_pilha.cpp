/**
* @file     palindromo_pilha.cpp
* @brief    Implementacao da funcao que verifica se uma palavra é palindromo
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    14/05/2017
*/

#include "palindromo_pilha.h"

#include <string>
using std::string;

#include <cstring>

#include "pilha.h"

/**
 * @brief Funcao que verifica se uma string é um palindromo, usando a estrutura de Pilha
 * @param tratada_s String para verificacao
 * @return Se a string é um palindromo ou não
 */
bool palindromo_pilha (string tratada_s)
{
    int tam_s = tratada_s.length ();

    // Copia a string para char
    char *palavra_c = new char [tam_s];
    tratada_s.copy (palavra_c, tam_s);

    int metade = tam_s/2;

    Pilha <char> pilha (metade);

    // Jogando as caracteres para a pilha ate metade da palavra
    for (int ii = 0; ii < metade; ii++)
        pilha.Push (palavra_c[ii]);

    char *aux = new char [metade];

    // Pega da pilha os caracteres (volta em ordem inversa)
    for (int ii = 0; ii < metade; ii++)
    {
        aux [ii] = pilha.Top ();
        pilha.Pop (); 
    }

    char *aux2 = new char [metade];

    // Se for impar ou par o numero de caracteres
    if (tam_s % 2 != 0)
    {
        // Pega a outra metade
        int temp = metade + 1;
        for (int ii = 0; ii < metade; ii++)
        {
            aux2 [ii] = palavra_c [temp];

            temp += 1;   
        }
    }
    else 
    {
        // Pega a outra metade
        int temp = metade;
        for (int ii = 0; ii < metade; ii++)
        {
            aux2 [ii] = palavra_c [temp];

            temp += 1;   
        }
    }

    // Compara as duas metades, se forem iguais, a string é um palindromo
    bool retorno = (strcmp (aux, aux2) == 0);

    delete [] aux;
    delete [] aux2;
    delete [] palavra_c;
    
    return retorno;
}