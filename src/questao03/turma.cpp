/**
 * @file	turma.cpp
 * @brief	Implementacao dos metodos da classe Turma
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	15/05/2017
 * @sa		turma.h
 */

#include "turma.h"

#include "ll_dupla_ordenada.h"
#include "aluno.h"

#include <iostream>
using std::endl;

#include <ostream>
using std::ostream;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;
using std::ofstream;

#include <cstdlib>
using std::atoi;
using std::atof;
using std::atol;

/**
 * @details Os valores de codigo é iniciado em branco, 
 *          quantidade de aluno, media das notas e total das notas sao inicializados com zero
 */
Turma::Turma () {
    codigo = " ";
    qnt_alunos = 0;
    media_notas = 0;
    notas_total = 0;
}

/**
 * @return Codigo da turma 
 */
string Turma::getCodigo () {
    return codigo;
}

/**
 * @details O metodo modifica o codigo da turma
 * @param   c Codigo da turma
 */
void Turma::setCodigo (string c) {
    codigo = c;
}

/**
 * @details O metodo adiciona um aluno a lista de alunos da turma 
 *          e faz o calculo da media das notas apos a adição do aluno
 * @param   m Numero da matricula do aluno
 * @param   n Nome do aluno
 * @param   f Numero de faltas do aluno
 * @param   nt Nota do aluno
 */
void Turma::AddAluno (long int m, string n, int f, float nt) {

    Aluno aux (m, n, f, nt);

    if (alunos.ExisteElemento (aux) == true)
    {
        cout << endl << "Aluno já cadastrado na turma !!!" << endl;

        return;
    }

    alunos.Inserir (aux);

    qnt_alunos += 1;

    notas_total += nt;
    media_notas = notas_total / qnt_alunos;
}

/**
 * @details O metodo adiciona alunos salvos em um arquivo a lista de alunos da turma 
 * @param   arquivo Arquivo com os dados dos alunos
 */
void Turma::AddAlunoArquivo (ifstream &arquivo) {

    string temp;

    int qntd;
    getline (arquivo, temp);
    qntd = atoi (temp.c_str ());

    for (int ii = 0; ii < qntd; ii++)
    {
        long int matricula;
        string nome;
        int faltas;
        float nota;

        getline (arquivo, temp, ';');
        matricula = atol (temp.c_str ());

        getline (arquivo, nome, ';');

        getline (arquivo, temp, ';');
        faltas = atoi (temp.c_str ());

        getline (arquivo, temp);
        nota = atof (temp.c_str ());

        AddAluno (matricula, nome, faltas, nota);
    }

    cout << qntd << " alunos carregados com sucesso !!!" << endl;
}

/**
 * @details O metodo remove um aluno da lista de alunos da turma 
 *          e faz o calculo da media das notas apos a remoção do aluno
 * @param   m Numero da matricula do aluno que será removido da turma
 */
void Turma::RemoveAluno (long int m) {

    Aluno aux;
    aux.setMatricula (m);

    Aluno temp = alunos.Buscar (aux);

    if (temp.getNome () != " ")
    {
        alunos.Remover (temp);

        cout << endl << "Aluno removido com sucesso !!!" << endl;

        qnt_alunos -= 1;

        notas_total -= temp.getNota ();
        media_notas = notas_total / qnt_alunos;
    }
    else
        cout << "Aluno não cadastrado !!!" << endl;
}

/** 
 * @details O operador e sobrecarregado para representar uma Turma 
 *			com as informacoes da turma e de seus alunos
 * @param	os Referencia para stream de saida
 * @param	t Referencia para o objeto Turma a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Turma &t) {

    os << endl << "======================= Turma " << t.codigo << " =======================" << endl << endl;
    os << "Quantidade de alunos: " << t.qnt_alunos << endl << endl;

    if (t.qnt_alunos == 0)
        os << "Não contém nenhum aluno na turma !!!" << endl;
    else
    {
        os << "Dados dos alunos:" << endl << endl;

        t.alunos.Mostrar ();

        os << "Media das notas: " << t.media_notas << endl;
    }

    return os;
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Turma::~Turma () {
    
}

/** 
 * @details O metodo escreve os dados de uma turma em um arquivo 
 * @param	arquivo Arquivo para gravação dos dados
 */
void Turma::Salvar (ofstream &arquivo) {

    arquivo << codigo << endl;
    arquivo << qnt_alunos << endl;
    arquivo << media_notas << endl;
    arquivo << notas_total << endl;

    arquivo << alunos.ElementoString () << endl;
}

/** 
 * @details O metodo carrega os dados de uma turma de um arquivo 
 * @param	arquivo Arquivo para leitura dos dados
 */
void Turma::Carregar (ifstream &arquivo) {

    string temp;

    // Ler o codigo da turma
    getline (arquivo, codigo);

    // Quantidade de alunos
    getline (arquivo, temp);
    int aux = atoi (temp.c_str ());

    // Media das notas
    getline (arquivo, temp);

    // Notas total
    getline (arquivo, temp);
    
    // Leitura dos alunos
    for (int ii = 0; ii < aux; ii++)
    {
        long int matricula;
        string nome;
        int faltas;
        float nota;

        getline (arquivo, temp, ';');
        matricula = atol (temp.c_str ());

        getline (arquivo, nome, ';');

        getline (arquivo, temp, ';');
        faltas = atoi (temp.c_str ());

        getline (arquivo, temp, ';');
        nota = atof (temp.c_str ());

        AddAluno (matricula, nome, faltas, nota);
    }
}

/** 
 * @details O operador e sobrecarregado para realizar a atribuicao
 *			de um instante de turma a outra
 * @param	t Instante de lista que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Autoreferencia para o objeto que invoca o metodo, com
 *			atributos atualizados
 */
Turma& Turma::operator = (Turma const &t) {

    codigo = t.codigo;
    qnt_alunos = t.qnt_alunos;
    media_notas = t.media_notas;
    notas_total = t.notas_total;
    
    alunos = t.alunos;
    
    return *this;
}