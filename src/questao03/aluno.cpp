/**
 * @file	aluno.cpp
 * @brief	Implementacao dos metodos da classe Aluno
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	15/05/2017
 * @sa		aluno.h
 */

#include "aluno.h"

#include <string>
using std::string;

#include <iostream>
using std::endl;

#include <ostream>
using std::ostream;

#include <sstream>
using std::ostringstream;

/**
 * @details Os valores de nome é iniciado com espaço, e matricula, faltas e nota sao inicializados com zero
 */
Aluno::Aluno () {
    matricula = 0;
    nome = " ";
    faltas = 0;
    nota = 0;
}

/**
 * @details Os valores dos dados do aluno sao recebidos por parametro
 * @param   m Numero da matricula
 * @param   n String para o nome
 * @param   f Numero de faltas
 * @param   nt Nota do aluno
 */
Aluno::Aluno (long int m, string n, int f, float nt) {
    matricula = m;
    nome = n;
    faltas = f;
    nota = nt;
}

/**
 * @return Matricula do aluno 
 */
long int Aluno::getMatricula () {
    return matricula;
}

/**
 * @details O metodo modifica a matricula do aluno
 * @param   m Numero da matricula para o aluno 
 */
void Aluno::setMatricula (long int m) {
    matricula = m;
}

/**
 * @return Nome do aluno 
 */
string Aluno::getNome () {
    return nome;
}

/**
 * @details O metodo modifica o nome do aluno
 * @param   n Nome para o aluno 
 */
void Aluno::setNome (string n) {
    nome = n;
}

/**
 * @return Numero de faltas do aluno 
 */
int Aluno::getFaltas () {
    return faltas;
}

/**
 * @details O metodo modifica o numero de faltas do aluno
 * @param   f Numero de faltas do aluno 
 */
void Aluno::setFaltas (int f) {
    faltas = f;
}

/**
 * @return Nota do aluno 
 */
float Aluno::getNota () {
    return nota;
}

/**
 * @details O metodo modifica a nota do aluno
 * @param   nt Nota do aluno 
 */
void Aluno::setNota (float nt) {
    nota = nt;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao de igualdade
 *			de um instante de Aluno a outro
 * @param	a Instante de Aluno que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Se um instante é igual ao outro
 */
bool Aluno::operator == (Aluno const a) {

    if (matricula == a.matricula)
        return true;

    return false;
}

/** 
 * @details O operador e sobrecarregado para realizar a verificacao se é menor
 *			um instante de Aluno a outro
 * @param	a Instante de Aluno que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Se um instante é menor que o outro
 */
bool Aluno::operator < (Aluno const a) {
    
    if (nome < a.nome)
        return true;
    
    return false;
}

/** 
 * @details O operador e sobrecarregado para representar um Aluno 
 *			com os dados do aluno
 * @param	os Referencia para stream de saida
 * @param	a Referencia para o objeto Aluno a ser impresso
 * @return	Referencia para stream de saida
 */
ostream& operator << (ostream &os, Aluno &a) {

    os << "Matricula: " << a.matricula << endl;
    os << "Nome: " << a.nome << endl;
    os << "Faltas: " << a.faltas << endl;
    os << "Nota: " << a.nota << endl;

    return os;
}

/** 
 * @details O metodo coloca os dados do aluno em uma string,
 *          com os dado separados por ';'
 * @return	String com os dados do aluno
 */
string Aluno::ToString () {

    ostringstream oss;

    oss << matricula << ";" << nome << ";" << faltas << ";" << nota << ";";

    return oss.str ();
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
Aluno::~Aluno () {
    
}