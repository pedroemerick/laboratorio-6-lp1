/**
* @file     funcs_dados.cpp
* @brief    Implementacao de algumas das funções que modificam os dados do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    15/05/2017
*/

#include "funcs_dados.h"

#include "turma.h"

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <sstream>
using std::ws;

#include <fstream>
using std::ofstream;
using std::ifstream;

#include "menus.h"

/**
* @brief Função que cria uma nova Turma, recebendo seus dados e atribuindo nas turmas
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void cadastra_turma (Turma *turmas, int &num_turmas)
{
    string codigo;
    cout << "Digite o codigo da turma: ";
    getline (cin >> ws, codigo);

    for (int ii = 0; ii < num_turmas; ii++)
    {
        if (turmas[ii].getCodigo () == codigo)
        {
            cout << "Turma já cadastrada !!!" << endl;

            return;
        }
    }

    num_turmas += 1;
    int temp = num_turmas - 1;

    turmas[temp].setCodigo (codigo);

    cout << "Turma '" << turmas[temp].getCodigo () << "' criada com sucesso !!!" << endl;
}

/**
* @brief Função que busca uma Turma pelo seu codigo
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
* @return Indice de onde se encontra a Turma
*/
int busca_turma (Turma *turmas, int num_turmas) 
{
    string codigo;
    int indice_turma;

    cout << "Digite o codigo da turma: ";
    getline (cin >> ws, codigo);

    int ii;
    for (ii = 0; ii < num_turmas; ii++)
    {
        if (turmas[ii].getCodigo () == codigo) {
            indice_turma = ii;
            break;
        }
    }
    if (ii == num_turmas)
    {
        cout << "Turma não cadastrada !!!" << endl;

        return -1;
    }

    return indice_turma;
}

/**
* @brief Função que adiciona alunos em uma Turma pelo terminal
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno_prog (Turma *turmas, int num_turmas) 
{
    int indice = busca_turma (turmas, num_turmas);
    int qntd_add;

    if (indice != -1) 
    {
        cout << endl << "Quantos alunos deseja cadastrar: ";
        cin >> qntd_add;

        cout << endl << "Insira os dados do(s) " << qntd_add << " aluno(s)" << endl;

        for (int ii = 0; ii < qntd_add; ii++) {
            cout << endl;

            long int matricula;
            cout << "Matricula: ";
            cin >> matricula;

            string nome;
            cout << "Nome: ";
            getline (cin >> ws, nome);

            int faltas;
            cout << "Quantidade de faltas: ";
            cin >> faltas;

            float nota;
            cout << "Nota: ";
            cin >> nota;

            turmas[indice].AddAluno (matricula, nome, faltas, nota);                
        }
    }   
}

/**
* @brief Função que adiciona alunos em uma Turma de um arquivo
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno_arq (Turma *turmas, int num_turmas)
{
    int indice = busca_turma (turmas, num_turmas);

    if (indice != -1)
    {
        string nome_arq = "./data/input/";
        string temp;

        cout << endl;
        cout << "Digite o nome do arquivo com os dados dos alunos (o arquivo deve estar na pasta ./data/input/): " << endl;
        cout << "Exemplo: alunos.csv" << endl << endl << "--> ";
        getline (cin >> ws, temp);

        nome_arq += temp;

        ifstream arquivo (nome_arq);

        if (!arquivo)
        {
            cout << endl << "Arquivo não encontrado !!!" << endl;

            return;
        }

        turmas[indice].AddAlunoArquivo (arquivo);

        arquivo.close ();
    }
}

/**
* @brief Função que adiciona alunos em uma Turma
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno (Turma *turmas, int num_turmas)
{
    int aux = 1;

    while (aux != 0)
    {
        int menu2 = menu_add_aluno (menu2);

        if (menu2 == 1)
        {
            add_aluno_prog (turmas, num_turmas);
            aux = 0;                    
        }
        else if (menu2 == 2)
        {
            add_aluno_arq (turmas, num_turmas);
            aux = 0;                    
        }
        else
            cout << "Opcão invalida !!! Digite novamente." << endl;
    }
}

/**
* @brief Função que carrega um arquivo com turmas e seus dados
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void carregar_turmas (Turma *turmas, int &num_turmas)
{
    ifstream arquivo ("./data/output/turmas.turma");

    if (!arquivo)
    {
        return;
    }

    string temp;

    // Numero de turmas
    getline (arquivo, temp);
    num_turmas = atoi (temp.c_str ());

    for (int ii = 0; ii < num_turmas; ii++)
    {
        turmas[ii].Carregar (arquivo);
        getline (arquivo, temp);
    }

    arquivo.close ();
}

/**
* @brief Função que salva em um arquivo todas as turmas e seus dados
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void salvar_turmas (Turma *turmas, int num_turmas)
{
    ofstream arquivo ("./data/output/turmas.turma");

    if (!arquivo)
    {
        exit (1);
    }

    arquivo << num_turmas << endl;

    for (int ii = 0; ii < num_turmas; ii++)
    {
        turmas[ii].Salvar (arquivo);
    }

    arquivo.close ();
}