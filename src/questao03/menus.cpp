/**
* @file     menus.cpp
* @brief    Implementacao das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    15/05/2017
*/

#include <iostream>
using std::endl;
using std::cout;
using std::cin;

#include "menus.h"

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu)
{
    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*                  Menu Principal                   *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      1) Criar turma                               *" << endl;
    cout << "*      2) Adicionar alunos a turma                  *" << endl;
    cout << "*      3) Listar dados e alunos de uma turma        *" << endl;
    cout << "*      4) Remover aluno de uma turma                *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*      0) Sair                                      *" << endl;
    cout << "*                                                   *" << endl;
    cout << "*****************************************************" << endl << endl;

    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> menu;
    cout << endl;

    return menu;
}

/**
* @brief Função que imprime o menu para a adicao de alunos com suas opcoes e recebe a escolha do usuario
* @param menu2 Valor do menu para a adicao de alunos
* @return Opcao escolhida pelo usuario
*/
int menu_add_aluno (int menu2)
{
    cout << endl;
    cout << "*****************************************************" << endl;
    cout << "*      1) Adicionar aluno pelo programa             *" << endl;
    cout << "*      2) Adicionar aluno por arquivo               *" << endl;
    cout << "*****************************************************" << endl << endl;            

    cout << "Digite a opção desejada:" << endl << "--> ";
    cin >> menu2;
    cout << endl;

    return menu2;
}