/**
* @file     main3.cpp
* @brief    Programa que manipulam turmas e seus dados
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    15/05/2017
*/

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include "ll_dupla_ordenada.h"
#include "turma.h"
#include "menus.h"
#include "funcs_dados.h"

/** 
 * @brief Função principal
 */
int main ()
{
    Turma turmas[100];
    int num_turmas = 0;

    carregar_turmas (turmas, num_turmas);

    int menu = -1;

    while (menu != 0) {

        menu = menu_principal (menu);

        if (menu == 1)
        {
            cadastra_turma (turmas, num_turmas);
        }
        else if (menu == 2)
        {
            add_aluno (turmas, num_turmas);
        }
        else if (menu == 3) 
        {
            int indice = busca_turma (turmas, num_turmas);

            if (indice != -1)
                cout << turmas[indice];
        }
        else if (menu == 4)
        {
            int indice = busca_turma (turmas, num_turmas);

            if (indice != -1)
            {
                long int matricula;
                cout << endl << "Digite a matricula do aluno que deseja remover da turma: ";
                cin >> matricula;

                turmas[indice].RemoveAluno (matricula);
            }
        }
        else if (menu < 0 || menu > 4)
        {
            cout << "Opção inválida !!! Digite novamente." << endl;
        }
    }

    salvar_turmas (turmas, num_turmas);

    return 0;
}