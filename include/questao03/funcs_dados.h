/**
* @file     funcs_dados.h
* @brief    Declaracao dos prototipos de algumas das funções que modificam os dados do programa
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    15/05/2017
*/

#ifndef FUNCS_DADOS_H
#define FUNCS_DADOS_H

#include "turma.h"

/**
* @brief Função que cria uma nova Turma, recebendo seus dados e atribuindo nas turmas
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void cadastra_turma (Turma *turmas, int &num_turmas);

/**
* @brief Função que busca uma Turma pelo seu codigo
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
* @return Indice de onde se encontra a Turma
*/
int busca_turma (Turma *turmas, int num_turmas); 

/**
* @brief Função que adiciona alunos em uma Turma pelo terminal
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno_prog (Turma *turmas, int num_turmas);

/**
* @brief Função que adiciona alunos em uma Turma de um arquivo
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno_arq (Turma *turmas, int num_turmas);

/**
* @brief Função que adiciona alunos em uma Turma
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void add_aluno (Turma *turmas, int num_turmas);

/**
* @brief Função que carrega um arquivo com turmas e seus dados
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void carregar_turmas (Turma *turmas, int &num_turmas);

/**
* @brief Função que salva em um arquivo todas as turmas e seus dados
* @param turmas Vetor com Turma
* @param num_turmas Quantidade de Turmas existentes
*/
void salvar_turmas (Turma *turmas, int num_turmas);

#endif