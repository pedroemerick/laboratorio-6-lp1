/**
 * @file	turma.h
 * @brief	Definicao da classe Turma, que representa um turma
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	14/05/2017
 */

#ifndef TURMA_H
#define TURMA_H

#include "ll_dupla_ordenada.h"
#include "aluno.h"

#include <ostream>
using std::ostream;

#include <string>
using std::string;

#include <fstream>
using std::ifstream;
using std::ofstream;

/**
 * @class   Turma turma.h
 * @brief   Classe que representa uma turma
 * @details Os atributos de uma Turma são o codigo da turma, Lista com os alunos, quantidade de alunos na turma
 *          media das notas dos alunos, e a soma das notas dos alunos
 */ 
class Turma {
    private:
        string codigo;                      /**< Codigo da turma */
        Lista <Aluno> alunos;               /**< Lista com os alunos da turma */
        int qnt_alunos;                     /**< Quantidade de alunos na turma */
        float media_notas;                  /**< Media das notas dos alunos */
        float notas_total;                  /**< Total das notas dos alunos */
    public:
        /** @brief Construtor padrao */
        Turma ();

        /** @brief Retorna o codigo da turma */
        string getCodigo ();

        /** @brief Modifica o codigo da turma */
        void setCodigo (string c);

        /** @brief Adiciona um aluno a Lista de alunos da turma */
        void AddAluno (long int m, string n, int f, float nt);

        /** @brief Adiciona alunos salvos em um arquivo a Lista de alunos da turma */
        void AddAlunoArquivo (ifstream &arquivo);

        /** @brief Remove um aluno da Lista de alunos da turma */
        void RemoveAluno (long int m);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Turma &t);

        /** @brief Destrutor padrao */
        ~Turma ();

        /** @brief Salva os dados da turma em um arquivo */
        void Salvar (ofstream &arquivo);

        /** @brief Carrega os dados da turma de um arquivo */
        void Carregar (ifstream &arquivo);

        /** @brief Sobrecarga do operador de atribuição */
        Turma& operator = (Turma const &t);
};

#endif