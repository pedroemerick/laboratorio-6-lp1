/**
 * @file	aluno.h
 * @brief	Definicao da classe aluno, que representa um aluno
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	15/05/2017
 */

#ifndef ALUNO_H
#define ALUNO_H

#include <string>
using std::string;

#include <ostream>
using std::ostream;

/**
 * @class   Aluno aluno.h
 * @brief   Classe que representa um aluno
 * @details Os atributos de um aluno são a matricula, o nome, as faltas e uma nota
 */ 
class Aluno {
    private:
        long int matricula;                 /**< Numero de matricula do aluno */
        string nome;                        /**< Nome do aluno */
        int faltas;                         /**< Quantidade de faltas */
        float nota;                         /**< Nota do aluno */
    public:
        /** @brief Construtor padrao */
        Aluno ();

        /** @brief Construtor parametrizado */
        Aluno (long int m, string n, int f, float nt);

        /** @brief Retorna a matricula do aluno */
        long int getMatricula ();
        
        /** @brief Modifica a matricula do aluno */
        void setMatricula (long int m);

        /** @brief Retorna o nome do aluno */        
        string getNome ();

        /** @brief Modifica o nome do aluno */  
        void setNome (string n);

        /** @brief Retorna o numero de faltas do aluno */  
        int getFaltas ();

        /** @brief Modifica o numero de faltas do aluno */  
        void setFaltas (int f);

        /** @brief Retorna a nota do aluno */  
        float getNota ();

        /** @brief Modifica a nota do aluno */  
        void setNota (float nt);

        /** @brief Sobrecarga do operador de igualdade */
        bool operator == (Aluno const a);

        /** @brief Sobrecarga do operador de menor */
        bool operator < (Aluno const a);

        /** @brief Sobrecarga do operador de insercao em stream */
        friend ostream& operator << (ostream &os, Aluno &a);

        /** @brief Retorna uma string com todos os dados do aluno */
        string ToString ();

        /** @brief Construtor padrao */
        ~Aluno ();
};

#endif