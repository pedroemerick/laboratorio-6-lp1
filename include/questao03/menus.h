/**
* @file     menus.h
* @brief    Declaracao dos prototipos das funcoes que imprimem os menus do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    11/05/2017
* @date	    15/05/2017
*/

#ifndef MENUS_H
#define MENUS_H

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu);

/**
* @brief Função que imprime o menu para a adicao de alunos com suas opcoes e recebe a escolha do usuario
* @param menu2 Valor do menu para a adicao de alunos
* @return Opcao escolhida pelo usuario
*/
int menu_add_aluno (int menu2);

#endif