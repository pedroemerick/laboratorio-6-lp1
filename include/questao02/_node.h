/**
 * @file	_node.h
 * @brief	Definicao e implementacao da classe Node, que representa um Node, 
 *          usado na lista para manipulacao dos dados
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	15/05/2017
 */

#ifndef _NODE_H
#define _NODE_H

#include <iostream>

/** 
 * @class 	Node _node.h
 * @brief 	Classe que representa um instante de Node
 * @details Os atributos de um instante de Node sao um apontador para a proxima posicao da lista,
 *          um apontador para a posicao anterior da lista, e uma variavel com o dado
 */
template < typename T >
class Node {
    private:
        T dado;                     /**< Dado desejado pelo usuário */
        Node <T> *prox;             /**< Apontador para a proxima posicao */
        Node <T> *ant;              /**< Apontador para a posicao anterior */
    public:
        /** @brief Construtor padrao */
        Node ();

        /** @brief Retorna o dado do Node */
        T getDado ();

        /** @brief Modifica o dado do Node */
        void setDado (T el);

        /** @brief Retorna a a proxima posicao da Lista */
        Node<T>* getProx ();

        /** @brief Modifica a proxima posicao da Lista */
        void setProx (Node <T> *p);

        /** @brief Retorna a posicao anterior da Lista */
        void setAnt  (Node <T> *a);

        /** @brief Modifica a posicao anterior da Lista */
        Node<T>* getAnt ();

        /** @brief Sobrecarga do operador de atribuição */
        Node<T>& operator = (Node<T> const &n);

        /** @brief Destrutor padrao */
        ~Node ();
};

/**
 * @details Os apontadores prox e ant sao iniciados com o vazio
 */
template < typename T >
Node<T>::Node () {
    prox = NULL;
    ant = NULL;
}

/**
 * @return Dado do Node
 */
template < typename T >
T Node<T>::getDado () {
    return dado;
}

/**
 * @details O metodo generico modifica o dado do Node
 * @param  el Dado para colocar no Node 
 */
template < typename T >
void Node<T>::setDado (T el) {
    dado = el;
}

/**
 * @return Ponteiro da proxima posicao da Lista
 */
template < typename T >
Node<T>* Node<T>::getProx () {
    return prox;
}

/**
 * @details O metodo generico modifica qual a proxima posicao da Lista
 * @param  p Nova proxima posicao da Lista 
 */
template < typename T >
void Node <T>::setProx (Node <T> *p) {
    prox = p;
}

/**
 * @return Ponteiro da posicao anterior da Lista
 */
template < typename T >
Node<T>* Node<T>::getAnt () {
    return ant;
}

/**
 * @details O metodo generico modifica qual a posicao anterior da Lista
 * @param  a Nova posicao anterior da Lista 
 */
template < typename T >
void Node <T>::setAnt (Node <T> *a) {
    ant = a;
}

/** 
 * @details O operador e sobrecarregado para realizar a atribuicao
 *			de um instante de node a outro
 * @param	n Instante de node que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Autoreferencia para o objeto que invoca o metodo, com
 *			atributos atualizados
 */
template < typename T >
Node<T>& Node<T>::operator = (Node<T> const &n) {

    dado = n.dado;
    prox = n.prox;
    ant = n.ant;

    return *this;
}

/**
 * @details Não ocorre nada pois nao tem variaveis alocadas
 */
template < typename T >
Node<T>::~Node () {
    
}


#endif