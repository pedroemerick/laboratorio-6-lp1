/**
* @file     menu.h
* @brief    Declaracao do prototipo da funcão que imprime o menu do programa e recebe a opcao do usuario
* @author   Pedro Emerick (p.emerick@live.com)
* @since    16/05/2017
* @date	    16/05/2017
*/

#ifndef MENU_H
#define MENU_H

/**
* @brief Função que imprime o menu principal com suas opcoes e recebe a escolha do usuario
* @param menu Valor do menu
* @return Opcao escolhida pelo usuario
*/
int menu_principal (int menu);

#endif