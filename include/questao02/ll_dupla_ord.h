/**
 * @file	ll_dupla_ord.h
 * @brief	Definicao e implementacao da classe Lista, que representa um tipo abstrato de dado chamado
 *          lista duplamente encadeada ordenada com sentinelas genérica 
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	11/05/2017
 * @date	15/05/2017
 */

#ifndef LL_DUPLA_ORD_H
#define LL_DUPLA_ORD_H

#include <iostream>
using std::cout;
using std::cin;
using std::endl;

#include <string>
using std::string;

#include "_node.h"

/** 
 * @class 	Lista ll_dupla_ord.h
 * @brief 	Classe que representa um instante de estrutura de Lista
 * @details Os atributos de um instante de Lista sao um apontador para o inicio da lista
 *          e um apontador para o fim da lista
 */
template < typename T >
class Lista {
    private:
        Node <T> *inicio;               /**< Apontador para o inicio da lista */  
        Node <T> *fim;                  /**< Apontador para o fim da lista */
    public:     
        /** @brief Construtor padrao */
        Lista ();

        /** @brief Inseri um elemento na lista */
        void Inserir (T el);

        /** @brief Mostra ao usuario toda a lista */
        void Mostrar ();

        /** @brief Remove um elemento da lista */
        void Remover (T el);

        /** @brief Busca um elemento na lista */
        T Buscar (T el);

        /** @brief Destrutor padrao */
        ~Lista ();

        /** @brief Retorna todos os dados em uma string */
        string ElementoString ();

        /** @brief Sobrecarga do operador de atribuição */
        Lista<T>& operator = (Lista<T> const &l);

        /** @brief Retorna se um elemento existe ou nao na lista */
        bool ExisteElemento (T el);
};

/**
 * @details Os apontadores inicio e fim sao iniciados com o vazio
 */
template < typename T >
Lista<T>::Lista () {
    inicio = NULL;
    fim = NULL;
}

/**
 * @details O metodo generico inseri um elemento na lista
 * @param   el Novo elemento para a inserir na lista 
 */
template < typename T >
void Lista<T>::Inserir (T el) {
    Node <T> *novo = new Node <T>;
    novo->setDado (el);

    // Caso seja o primeiro elemento
    if (inicio == NULL)
    {
        inicio = novo;
        fim = novo;

        return;
    }

    Node <T> *atual = inicio;

    while (atual != NULL)
    {
        // Verifica se o elemento da posicao é menor do que o elemento que desejo inserir, para colocar na posicao correta
        if (atual->getDado () < el)
            atual = atual->getProx ();
        else
        {
            novo->setProx (atual);

            // Olha se vai inserir na primeira posicao ou nao, verificando se o anterior da posicao para 
            // insercao é nulo
            if (atual->getAnt () != NULL)               // se nao for na primeira posicao    
            {
                novo->setAnt (atual->getAnt ());
                atual->getAnt ()->setProx (novo);
                atual->setAnt (novo);

                return;
            }
            else                                        //se for na primeira posicao
            {
                atual->setAnt (novo);
                inicio = novo;

                return;
            }
        }
    }

    // se deve inserir na ultima posicao
    fim->setProx (novo);
    novo->setAnt (fim);
    fim = novo;
}

/**
 * @details O metodo generico imprimi para o usuario todos os dados da lista
 */
template < typename T >
void Lista<T>::Mostrar () // mostra todos os elementos da lista
{
    Node <T> *aux = inicio;

    if (inicio == NULL)
        cout << "A lista não contém elemento !!!" << endl;
    else
    {
        while(aux) // percorre a lista
        {
            T temp = aux->getDado ();
            cout << temp << " ";
            aux = aux->getProx();
        }
    }
}

/**
 * @details O metodo generico remove um elemento na lista
 * @param   el Elemento para ser retirado da lista 
 */
template < typename T >
void Lista<T>::Remover (T el)
{
    Node <T> *aux = inicio;

    // Pesquisa posicao do elemento
    while (aux != NULL)
    {
        if (aux->getDado () == el)
            break;
        else 
            aux = aux->getProx ();
    }

    // Se nao achar o elemento
    if (aux == NULL)
    {
        cout << "Elemento não existe !!!" << endl;
    }
    else if (aux == inicio) // Se o elemento estiver no inicio
    {
        if (aux->getAnt () == NULL && aux->getProx () == NULL)
        {
            inicio = NULL;

            delete aux;
        }
        else
        {
            aux->getProx()->setAnt (NULL);
            inicio = aux->getProx ();

            delete aux;
        }
    }
    else if (aux == fim) // Se o elemento estiver no fim
    {
        aux->getAnt()->setProx (NULL);
        fim = aux->getAnt ();

        delete aux;
    }
    else // Se estiver em qualquer outra posicao
    {
        aux->getProx()->setAnt(aux->getAnt());
        aux->getAnt()->setProx(aux->getProx());

        delete aux;
    }
}

/**
 * @details O metodo generico busca um elemento na lista
 * @param   el Elemento que está procurando na lista
 * @return Retorna o elemento se encontrado, se não retorna o elemento sem todos os dados
 */
template < typename T >
T Lista<T>::Buscar (T el) {

    Node <T> *aux = inicio;

    // Pesquisa posicao do elemento
    while (aux != NULL)
    {
        if (aux->getDado () == el)
            return aux->getDado ();
        else 
            aux = aux->getProx ();
    }

    return el;
}

/**
 * @details É liberado todos os elementos da lista se alocado
 */
template < typename T >
Lista<T>::~Lista () {

    while (inicio != NULL)
    {
        Node <T> *aux = inicio;
        inicio = inicio->getProx ();

        delete aux;
    }
}

/**
 * @details O metodo generico faz a juncao de todos os dados da lista em uma string
 * @return Retorna a string com todos os dados da lista
 */
template < typename T >
string Lista<T>::ElementoString ()
{
    Node <T> *aux = inicio;

    string elementos;

    if (inicio != NULL)
    {
        while(aux) // percorre a lista
        {
            elementos += aux->getDado ().ToString ();
            aux = aux->getProx();
        }
    }

    return elementos;
}

/** 
 * @details O operador e sobrecarregado para realizar a atribuicao
 *			de um instante de lista a outra
 * @param	l Instante de lista que sera atribuido ao objeto que invoca
 *			o metodo
 * @return	Autoreferencia para o objeto que invoca o metodo, com
 *			atributos atualizados
 */
template < typename T >
Lista<T>& Lista<T>::operator = (Lista<T> const &l) {

    inicio = l.inicio;
    fim = l.fim;

    return *this;
}

/**
 * @details O metodo generico verifica se um elemento existe na lista
 * @param   el Elemento para verificação
 * @return  Retorna se o elemento existe na lista ou não
 */
template < typename T >
bool Lista<T>::ExisteElemento (T el) {

    Node <T> *aux = inicio;

    // Pesquisa pelo elemento
    while (aux != NULL)
    {
        if (aux->getDado () == el)
            return true;
        else 
            aux = aux->getProx ();
    }

    return false;
}

#endif