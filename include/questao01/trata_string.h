/**
* @file     trata_string.h
* @brief    Declaracao dos prototipos das funcoes que fazem o tratamento de strings
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    14/05/2017
*/

#ifndef TRATA_STRING_H
#define TRATA_STRING_H

#include <string>
using std::string;

/**
 * @brief Funcao que converte todos os caracteres para letra minuscula
 * @param s String a ser convertida
 * @param t Tamanho da string
 */
void to_minusculo (string &s, int t);

/**
 * @brief Funcao que remove espacos e sinais de pontuacao de uma string
 * @param s String com espacos e sinais de pontuacao a remover
 * @param t Tamanho da string
 */
void retira_especiais (string &s, int t);

/**
 * @brief Funcao que remove os acentos dos caracteres de uma string
 * @param s String com acentos a remover
 */
void retira_acentos (string &s);

/**
 * @brief Funcao que faz o tratamento da string
 * @param s String para tratamento
 */
void tratamento_string (string &s);

#endif