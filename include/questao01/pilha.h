/**
 * @file	pilha.h
 * @brief	Definicao da classe Pilha, que representa uma pilha e a implementação dos seus metodos genericos
 * @author	Pedro Emerick (p.emerick@live.com)
 * @since	09/05/2017
 * @date	14/05/2017
 */

#ifndef PILHA_H
#define PILHA_H

#include <iostream>
using std::cout;
using std::endl;

/**
 * @class   Pilha pilha.h
 * @brief   Classe que representa uma pilha
 * @details Os atributos de uma pilha são o vetor com dados, o tamanho maximo do vetor 
 *          e a quantidade de dados no vetor
 */
template < typename T >
class Pilha {
    private:
        T *vetor;                   /**< Vetor de dados */
        int tamanho_max;            /**< Tamanho maximo do vetor */
        int qntd;                   /**< Tamanho ja usado pelo vetor */
    public:
        /** @brief Construtor parametrizado */
        Pilha (int tm);

        /** @brief Destrutor padrão */
        ~Pilha ();
        
        /** @brief Inseri um elemento no vetor de dados */
        void Push (T el);

        /** @brief Remove o ultimo elemento do vetor */
        void Pop ();

        /** @brief Retorna o ultimo elemento do vetor */
        T Top ();
};

/**
 * @details O vetor é alocado dinamicamente com o seu tamanho maximo recebido como parametro,
 *          o tamanho maximo recebe o valor passado, e qntd iniciado com zero
 * @param   tm Tamanho maximo para o vetor
 */
template < typename T >
Pilha<T>::Pilha (int tm) {
    vetor = new T [tm];
    tamanho_max = tm;
    qntd = 0;
}

/**
 * @details O vetor de dados é liberado
 */
template < typename T >
Pilha<T>::~Pilha () {
    delete [] vetor;
}

/**
 * @details O metodo generico inseri o elemento passado na proxima posicao livre do vetor
 * @param   el Elemento para inserir no vetor 
 */
template < typename T >
void Pilha<T>::Push (T el) {
    if (qntd == tamanho_max)
        cout << "A pilha está cheia !!!" << endl;
    else 
    {
        vetor [qntd] = el;
        qntd += 1;
    }
}

/**
 * @details O metodo generico remove o ultimo elemento colocado no vetor
 */
template < typename T >
void Pilha<T>::Pop () {
    if (qntd == 0)
        cout << "A pilha está vazia !!!" << endl;
    else
        qntd -= 1;
}

/**
 * @return O ultimo elemento inserido na pilha
 */
template < typename T >
T Pilha<T>::Top () {
    if (qntd == 0) {
        cout << "A pilha está vazia !!!" << endl;
        
        exit (1);
    }
    else 
        return vetor[qntd - 1];
}

#endif