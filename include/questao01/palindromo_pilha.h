/**
* @file     palindromo_pilha.h
* @brief    Declaracao do prototipo da funcao que verifica se uma palavra é palindromo
* @author   Pedro Emerick (p.emerick@live.com)
* @since    09/05/2017
* @date	    14/05/2017
*/

#ifndef PALINDROMO_PILHA_H
#define PALINDROMO_PILHA_H

#include <string>
using std::string;

/**
 * @brief Funcao que verifica se uma string é um palindromo, usando a estrutura de Pilha
 * @param tratada_s String para verificacao
 * @return Se a string é um palindromo ou não
 */
bool palindromo_pilha (string tratada_s);

#endif