LIB_DIR = ./lib
INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc
TEST_DIR = ./test

CC = g++
CPPFLAGS = -Wall -pedantic -ansi -std=c++11

RM = rm -rf
RM_TUDO = rm -fr

.PHONY: all clean debug doc doxygen valgrind1 valgrind2 valgrind3

all: init questao01 questao02 questao03

debug: CPPFLAGS += -g -O0
debug: all

valgrind1: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/palindromo
valgrind2: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/lista
valgrind3: 
	valgrind --leak-check=full --show-reachable=yes -v ./bin/turmas

init:
	@mkdir -p $(BIN_DIR)/
	@mkdir -p $(OBJ_DIR)/questao01/
	@mkdir -p $(OBJ_DIR)/questao02/
	@mkdir -p $(OBJ_DIR)/questao03/

# QUESTAO 01

questao01: CPPFLAGS += -I. -I$(INC_DIR)/questao01
questao01: $(OBJ_DIR)/questao01/main.o $(OBJ_DIR)/questao01/trata_string.o $(OBJ_DIR)/questao01/palindromo_pilha.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/palindromo $^
	@echo "*** [Executavel palindromo criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao01/main.o: $(SRC_DIR)/questao01/main.cpp $(INC_DIR)/questao01/trata_string.h $(INC_DIR)/questao01/palindromo_pilha.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/trata_string.o: $(SRC_DIR)/questao01/trata_string.cpp $(INC_DIR)/questao01/trata_string.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao01/palindromo_pilha.o: $(SRC_DIR)/questao01/palindromo_pilha.cpp $(INC_DIR)/questao01/palindromo_pilha.h $(INC_DIR)/questao01/pilha.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 02

questao02: CPPFLAGS += -I. -I$(INC_DIR)/questao02
questao02: $(OBJ_DIR)/questao02/main2.o $(OBJ_DIR)/questao02/menu.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/lista $^
	@echo "*** [Executavel lista criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao02/main2.o: $(SRC_DIR)/questao02/main2.cpp $(INC_DIR)/questao02/ll_dupla_ord.h $(INC_DIR)/questao02/menu.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao02/menu.o: $(SRC_DIR)/questao02/menu.cpp $(INC_DIR)/questao02/menu.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

# QUESTAO 03

questao03: CPPFLAGS += -I. -I$(INC_DIR)/questao03
questao03: $(OBJ_DIR)/questao03/main3.o $(OBJ_DIR)/questao03/aluno.o $(OBJ_DIR)/questao03/turma.o $(OBJ_DIR)/questao03/menus.o $(OBJ_DIR)/questao03/funcs_dados.o
	@echo "====================================================="
	@echo "Ligando o alvo $@"
	@echo "====================================================="		
	$(CC) $(CPPFLAGS) -o $(BIN_DIR)/turmas $^
	@echo "*** [Executavel turmas criado em $(BIN_DIR)] ***"
	@echo "====================================================="

$(OBJ_DIR)/questao03/main3.o: $(SRC_DIR)/questao03/main3.cpp $(INC_DIR)/questao03/ll_dupla_ordenada.h $(INC_DIR)/questao03/turma.h $(INC_DIR)/questao03/menus.h $(INC_DIR)/questao03/funcs_dados.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao03/aluno.o: $(SRC_DIR)/questao03/aluno.cpp $(INC_DIR)/questao03/aluno.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao03/turma.o: $(SRC_DIR)/questao03/turma.cpp $(INC_DIR)/questao03/turma.h $(INC_DIR)/questao03/ll_dupla_ordenada.h $(INC_DIR)/questao03/aluno.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao03/menus.o: $(SRC_DIR)/questao03/menus.cpp $(INC_DIR)/questao03/menus.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

$(OBJ_DIR)/questao03/funcs_dados.o: $(SRC_DIR)/questao03/funcs_dados.cpp $(INC_DIR)/questao03/funcs_dados.h $(INC_DIR)/questao03/turma.h
	$(CC) -c $(CPPFLAGS) -o $@ $<

doxygen:
	doxygen -g

doc:
	@echo "====================================================="
	@echo "Limpando pasta $(DOC_DIR)"
	@echo "====================================================="
	$(RM_TUDO) $(DOC_DIR)/*
	@echo "====================================================="
	@echo "Gerando nova documentação na pasta $(DOC_DIR)"
	@echo "====================================================="
	doxygen Doxyfile

clean:
	@echo "====================================================="
	@echo "Limpando pasta $(BIN_DIR), $(OBJ_DIR) e $(IMG_DIR)"
	@echo "====================================================="
	$(RM) $(BIN_DIR)/*
	$(RM) $(OBJ_DIR)/questao01/*
	$(RM) $(OBJ_DIR)/questao02/*
	$(RM) $(OBJ_DIR)/questao03/*
